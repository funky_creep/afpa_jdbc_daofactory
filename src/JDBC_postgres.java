import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import exceptions.ExceptionSQL;
import model.DAOFactory;
import model.Stockeur;
import model.beans.Produit;
import model.dao.DAOBoisson;
import model.utils.Connexion;

public class JDBC_postgres {

	public static void main(String[] args) {

	         // On cr�e la connexion
	         Connexion connect = new Connexion("jdbc:postgresql://localhost:5432/db_machinecafe", "postgres", "postgres", "org.postgresql.Driver");
	         DAOFactory monUsineADao = new DAOFactory(connect);
	         
	         // Recherche du produit ayant l'id 2
	         System.out.println("FindById() :");
	         try {
				System.out.println(monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(2).getNom());
	         } catch (ExceptionSQL e) {
				System.out.println(e.getMessage());
	         } finally {
	        	 System.out.println("");
	         }
	         
	         // Recherche de tous les produits
	         System.out.println("FindAll() :");
	         try {
				Stockeur<Produit> stockProduits = monUsineADao.getDAO(DAOFactory.DAOBoisson).findAll();
				for (Produit p : stockProduits.getStock().values()) {
					System.out.println(p.getNom());
				}
		     } catch (ExceptionSQL e) {
				System.out.println(e.getMessage());
		     } finally {
		    	 System.out.println("");
		     }
	         
	         // Cr�ation d'un nouveau produit
	         System.out.println("Create() :");
	         try {
				System.out.println(monUsineADao.getDAO(DAOFactory.DAOBoisson).create(new Produit(1, "Orangina", 120, 5)).getNom());
				//Produit test = (monUsineADao.getDAO(DAOFactory.DAOBoisson).create(new Produit(1, "Orangina", 120, 5));
			} catch (ExceptionSQL e) {
				System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			}
	         
	         // Update d'un produit de la table
	         System.out.println("Update() :");
			try {
				//Produit produitTestUpdate = monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(1);
				//produitTestUpdate.setNom("Pepsi");
				monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(1).setNom("Pepsi");
				System.out.println(monUsineADao.getDAO(DAOFactory.DAOBoisson).update(monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(1)).getNom());
			} catch (ExceptionSQL e) {
				System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			}
	         
	         // Suppression d'un produit (avec le produit) de la table
			/*System.out.println("DeleteWithProduct() :");
			try {
				Produit produitTestUpdate = monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(1);
				monUsineADao.getDAO(DAOFactory.DAOBoisson).delete(produitTestUpdate);
				System.out.println(monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(1).getNom());
			} catch (ExceptionSQL e) {
				System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			} */
			
			// Suppression d'un produit de la table avec son id 
			System.out.println("DeleteWithId() :");
			try {
				monUsineADao.getDAO(DAOFactory.DAOBoisson).delete(1);
				System.out.println(monUsineADao.getDAO(DAOFactory.DAOBoisson).findById(1));
			} catch (ExceptionSQL e) {
				System.out.println(e.getMessage());
			} finally {
				System.out.println("");
			}
	         
	         // On ferme la connexion
	         connect.closeConnection();
	}

}
