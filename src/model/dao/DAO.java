package model.dao;

import exceptions.ExceptionSQL;
import model.Stockable;
import model.Stockeur;
import model.beans.Produit;
import model.utils.Connexion;

public abstract class DAO<T extends Stockable> {
	
	/**
	 * ATTRIBUTS
	 */
	private Connexion cxion = null;

	
	/**
	 * CONSTRUCTEUR
	 * 
	 * @param cxion
	 */
	public DAO(Connexion pCxion) {
		this.cxion = pCxion;
	}
	
	/**
	 * GETTERS & SETTERS
	 */
	
	protected Connexion getCxion() {
		return this.cxion;
	}
	
	

	/**
	 * 
	 * METHODES
	 * @throws ExceptionSQL 
	 * 
	 */
	
	public abstract T 			create(T 	objACreer) throws ExceptionSQL;
	public abstract T 			update(T 	objAModifier) throws ExceptionSQL;
	public abstract void 		delete(int 	idADeleter) throws ExceptionSQL;
	public abstract void 		delete(T 	objADeleter) throws ExceptionSQL;
	
	public abstract T 			findById(int idATrouver) throws ExceptionSQL;
	public abstract Stockeur<T> findAll() throws ExceptionSQL;
	
}
