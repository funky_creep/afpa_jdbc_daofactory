package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.util.PSQLException;

import exceptions.ExceptionSQL;
import model.Stockeur;
import model.beans.Produit;
import model.utils.Connexion;

public class DAOBoisson extends DAO<Produit> {

	public DAOBoisson(Connexion pCxion) {
		super(pCxion);
	}

	// Insertion d'un �l�ment dans la table produit
	@Override
	public Produit create(Produit objACreer) throws ExceptionSQL {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("INSERT INTO boisson(id, nom, prix, qte) VALUES (?, ?, ?, ?);");
			ps.setInt(1, objACreer.getId());
			ps.setString(2, objACreer.getNom());
			ps.setInt(3, objACreer.getValeur());
			ps.setInt(4, objACreer.getQte());
			ps.executeUpdate();
			return objACreer;
		}
		catch(SQLException e) {
			throw new ExceptionSQL("Cette boisson existe d�j� dans la base de donn�es");
		}
	}

	// Modifier un �l�ment de la table produit
	@Override
	public Produit update(Produit objAModifier) throws ExceptionSQL {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("UPDATE boisson SET nom = ?, prix = ?, qte = ? WHERE id = ?;");
			ps.setString(1, objAModifier.getNom());
			ps.setInt(2, objAModifier.getValeur());
			ps.setInt(3, objAModifier.getQte());
			ps.setInt(4, objAModifier.getId());
			ps.executeUpdate();
			return objAModifier;
		} catch (SQLException e) {
			throw new ExceptionSQL("Ce produit n'existe pas");
		}
	}

	// Supprimer un �l�ment de la table produit avec son id
	@Override
	public void delete(int idADeleter) throws ExceptionSQL {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("DELETE FROM boisson WHERE id = ?;");
			ps.setInt(1, idADeleter);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new ExceptionSQL("Ce produit n'existe pas");
		}
	}

	// Supprimer un �l�ment de la table produit avec le produit
	@Override
	public void delete(Produit objADeleter) throws ExceptionSQL {
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("DELETE FROM boisson WHERE id = ?;");
			ps.setInt(1, objADeleter.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new ExceptionSQL("Ce produit n'existe pas");
		}
		
	}

	// Trouver un �l�ment de la table produit avec son id
	@Override
	public Produit findById(int idATrouver) throws ExceptionSQL {
		String req = "SELECT * FROM boisson WHERE id = "+idATrouver;
		try {
			PreparedStatement stmt 	= getCxion().getC().prepareStatement(req);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return new Produit(rs.getInt("id"), rs.getString("nom"), rs.getInt("prix"), rs.getInt("qte"));					
		} catch (SQLException e) {
			throw new ExceptionSQL("Ce produit n'existe pas");
		}
	}

	// Trouver tous les �l�ments de la table produit
	@Override
	public Stockeur<Produit> findAll() throws ExceptionSQL {
		Stockeur<Produit> stockBoissons = new Stockeur<Produit>();
		try {
			PreparedStatement ps = getCxion().getC().prepareStatement("SELECT * from boisson;");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stockBoissons.addItem(new Produit(rs.getInt("id"), rs.getString("nom"), rs.getInt("prix"), rs.getInt("qte")));
			}
			return stockBoissons;
		} catch (SQLException e) {
			throw new ExceptionSQL("Cette table n'existe pas");
		}
	}

}
