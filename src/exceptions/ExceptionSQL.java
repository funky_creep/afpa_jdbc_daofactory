package exceptions;

import java.sql.SQLException;

public class ExceptionSQL extends SQLException {

	private static final long serialVersionUID = -3125848476216774052L;

	public ExceptionSQL(String msg) {
		super(msg);
	}
	
}
