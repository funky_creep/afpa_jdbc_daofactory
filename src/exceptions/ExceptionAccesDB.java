package exceptions;

import java.sql.SQLException;

public class ExceptionAccesDB extends SQLException {

	private static final long serialVersionUID = -1499656337734035129L;

	public ExceptionAccesDB(String msg) {
		super(msg);
	}
	
}
